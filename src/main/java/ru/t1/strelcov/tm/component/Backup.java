package ru.t1.strelcov.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.strelcov.tm.bootstrap.Bootstrap;

public class Backup extends Thread {

    @NotNull
    private final Bootstrap bootstrap;

    public Backup(@NotNull Bootstrap bootstrap) {
        setDaemon(true);
        this.bootstrap = bootstrap;
    }

    @SneakyThrows
    @Override
    public void run() {
        while (true) {
            save();
            Thread.sleep(30000);
        }
    }

    private void load() {
        bootstrap.parseCommand("backup-load");
    }

    private void save() {
        bootstrap.parseCommand("backup-save");
    }

    public void init() {
        load();
        start();
    }

}
