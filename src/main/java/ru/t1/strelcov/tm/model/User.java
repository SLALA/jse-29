package ru.t1.strelcov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.enumerated.Role;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

@Getter
@Setter
@NoArgsConstructor
public final class User extends AbstractEntity {
    @NotNull
    private String login = "";
    @NotNull
    private String passwordHash;
    @Nullable
    private String email = "";
    @Nullable
    private String firstName = "";
    @Nullable
    private String lastName = "";
    @Nullable
    private String middleName = "";
    @Nullable
    private Role role = Role.USER;
    @NotNull
    private Boolean locked = false;

    public User(@NotNull final String login, @NotNull final String passwordHash) {
        this.login = login;
        this.passwordHash = passwordHash;
    }

    public User(@NotNull final String login, @NotNull final String passwordHash, @Nullable final String email) {
        this.login = login;
        this.passwordHash = passwordHash;
        this.email = email;
    }

    public User(@NotNull final String login, @NotNull final String passwordHash, @Nullable final Role role) {
        this.login = login;
        this.passwordHash = passwordHash;
        this.role = role;
    }

    @NotNull
    @Override
    public String toString() {
        return getId() + (login.isEmpty() ? "" : " : " + login);
    }

}
