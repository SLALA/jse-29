package ru.t1.strelcov.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.dto.Domain;
import ru.t1.strelcov.tm.enumerated.Role;

import java.nio.file.Files;
import java.nio.file.Paths;

public class DataYamlLoadFasterXMLCommand extends AbstractDataCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String name() {
        return "data-yaml-load-fasterxml";
    }

    @Override
    @NotNull
    public String description() {
        return "Load entities data from yaml file.";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA YAML LOAD]");
        @NotNull final String json = new String(Files.readAllBytes(Paths.get(FILE_FASTERXML_YAML)));
        @NotNull final ObjectMapper objectMapper = new ObjectMapper(new YAMLFactory());
        @Nullable final Domain domain = objectMapper.readValue(json, Domain.class);
        setDomain(domain);
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[]{
                Role.ADMIN
        };
    }

}
