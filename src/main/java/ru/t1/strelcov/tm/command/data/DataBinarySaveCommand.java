package ru.t1.strelcov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.dto.Domain;
import ru.t1.strelcov.tm.enumerated.Role;

import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;

public class DataBinarySaveCommand extends AbstractDataCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String name() {
        return "data-binary-save";
    }

    @Override
    @NotNull
    public String description() {
        return "Save entities data to binary file.";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA BINARY SAVE]");
        @NotNull final Domain domain = getDomain();

        @NotNull final File file = new File(FILE_BINARY);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
        @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(domain);
        objectOutputStream.close();
        fileOutputStream.close();
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[]{
                Role.ADMIN
        };
    }

}
