package ru.t1.strelcov.tm.command.user;

import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.command.AbstractCommand;
import ru.t1.strelcov.tm.model.User;

public abstract class AbstractUserCommand extends AbstractCommand {

    protected void showUser(@Nullable final User user) {
        if (user == null) return;
        System.out.println("[Id]: " + user.getId());
        System.out.println("[Login]: " + user.getLogin());
        System.out.println("[Email]: " + user.getEmail());
        System.out.println("[First Name]: " + user.getFirstName());
        System.out.println("[Last Name]: " + user.getLastName());
        System.out.println("[Middle Name]: " + user.getMiddleName());
    }

}
