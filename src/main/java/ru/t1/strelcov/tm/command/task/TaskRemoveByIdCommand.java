package ru.t1.strelcov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.service.ITaskService;
import ru.t1.strelcov.tm.exception.entity.TaskNotFoundException;
import ru.t1.strelcov.tm.model.Task;
import ru.t1.strelcov.tm.util.TerminalUtil;

public final class TaskRemoveByIdCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-remove-by-id";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove task by id.";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        System.out.println("[REMOVE TASK BY ID]");
        System.out.println("ENTER TASK ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        taskService.removeById(userId, id);
    }

}
